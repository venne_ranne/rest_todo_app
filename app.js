var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var bodyParser = require('body-parser');
var fs = require('fs');

var pg = require('pg');
//pg.defaults.ssl = true;
var connectionString = "postgres://yappvivi:12345678@depot:5432/yappvivi_jdbc" || process.env.DATABASE_URL;
// "postgres://localhost:5432/yappvivi_jdbc"

app.use(function(req, res, next){
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// static files such as css, js files
app.use(express.static('./views/main'));

// listen to port
app.listen(port, function(){
  console.log('You are listening to port 8080!');
});


// send the html file
app.get('/', function (req, res){
    res.writeHead(200, {'Content-type': 'text/html'});
    fs.readFile('./views/main/index.html', function(error, data){
      if (error){
        res.writeHead(404);
        res.write('File not found!');
      } else{
        res.write(data);
      }
      res.end();
    })
});


// get all the rows from the database
app.get('/todos', function (req, res){
  var results = [];
  var query, queryCmd;
  pg.connect(connectionString, (err, client, done) => {
    if (err){
      done();
      console.log("get all to-do tasks error");
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    queryCmd = 'SELECT * FROM todo;';
    query = client.query(queryCmd);
    query.on('row', (row) => {
      results.push(row);
    });
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
});


// delete an item using id
app.delete("/todos/:id", function (req, res){
  var item = req.params.id;
  var query, queryCmd;

  pg.connect(connectionString, (err, client, done) => {
    if (err){
      done();
      console.log("delete to-do task using id error");
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    queryCmd = 'DELETE FROM todo WHERE id=($1);';
    query = client.query(queryCmd, [item]);
    query.on('end', () => {
      console.log("Successfully deleted a task.");
      console.log("delete item => id: "+item);
      done();
      res.send("success");
    });
  });
});


// add data into database
app.post("/todos", function (req, res){
  var data = {task:req.body.task, user:req.body.user}
  var results = [];
  var query, queryCmd;
  pg.connect(connectionString, (err, client, done) => {
    if (err){
      done();
      console.log("insert new to-do task error");
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }

    // insert a new todo task into the todo table
    queryCmd = 'INSERT INTO todo (item, username) VALUES ($1, $2)';
    query = client.query(queryCmd, [data.task, data.user]);

    // get the last item in the todo table based on the max id
    queryCmd = 'SELECT * FROM todo WHERE id = (SELECT MAX(id) FROM todo);';
    query = client.query(queryCmd);

    query.on('row', (row) => {
      console.log("Successfully added a new task.");
      console.log("insert values => item: "+row.item+ ", username: "+row.username);
      results.push(row);
    });
    query.on('end', function(){
      done();
      return res.json(results);
    });
  });
});


// update an item, using id to find the item
// can be used when the edit dialog box pop-up
// also can be used when the done box is clicked
app.put("/todos/:id", function (req, res){
  var updateData = {task:req.body.task, user:req.body.user, complete:req.body.complete}
  var id = req.params.id;
  var query, queryCmd;
  var results = [];

  pg.connect(connectionString, (err, client, done) => {
    if (err){
      done();
      console.log("update a to-do task using id error");
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    queryCmd = 'UPDATE todo SET item=($1), username=($2), complete=($3) WHERE id=($4);';
    query = client.query(queryCmd, [updateData.task,  updateData.user,  updateData.complete, id]);

    queryCmd = 'SELECT * FROM todo WHERE id = ($1);';
    query = client.query(queryCmd, [id]);

    query.on('row', (row) => {
      console.log("updated values => item: "+row.item+ ", username: "+row.username);
      results.push(row);
    });

    query.on('end', function(){
      console.log("Successfully updated a task.");
      done();
      return res.json(results);
    });
  });
});


// update an item, using id to find the item
// move item to either completed-list or todo-list based on complete parameter
app.put("/todos/:id/:complete", function (req, res){
  var id = req.params.id;
  var updateData = {complete:req.params.complete}
  var query, queryCmd;
  var results = [];

  pg.connect(connectionString, (err, client, done) => {
    if (err){
      done();
      console.log("update a to-do task using id and complete params when mouse dragged error");
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    console.log("move item to done list => "+ "id: "+id);
    queryCmd = 'UPDATE todo SET complete=($1) WHERE id=($2);';
    query = client.query(queryCmd, [updateData.complete, id]);

    queryCmd = 'SELECT * FROM todo WHERE id = ($1);';
    query = client.query(queryCmd, [id]);

    query.on('row', (row) => {  // complete should be print true
      console.log("Successfully moved item to done list => item: "+row.item+ ", username: "+row.username+", complete: "+row.complete);
      results.push(row);
    });

    query.on('end', function(){
      console.log("Successfully moved a task to the done list.");
      done();
      return res.json(results);
    });
  });
});
