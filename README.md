# **README for rest_todo_app** ##
This repo is created for NWEN304 individual project part 2 that implemented a REST service (supports a PostgreSQL database) for a TODO web application. The REST service is implemented using Node.js and Express. The HTTP requests are implemented in Ajax calls that send and receive data from the REST service. The REST service designed to update/delete information from the database using ID of the task as each task has its own unique ID. To test if the REST service running properly, a set of curl test cases were used for each method.

## **Running Locally** ##
Make sure you have [Node.js](http://nodejs.org/) and [PostgreSQL](https://postgresapp.com) app installed.

## **Quick Start** ##
1. Clone the repo
2. Start your Postgres server
3. Create a table called todo: create table todo (id serial primary key, item varchar(255), username varchar(255), complete boolean default 'false');
3. Change directory to the folder: $ cd rest_app_todo
4. Start the server: $ node app.js

## **REST Interface Documentation** ##
#### ** GET method (URL: /todos)**
To get all the todo tasks from the todo table. All the results will return in an array.

#### ** POST method (URL: /todos)**
To insert a single todo task into the todo table using the task name and user name. If it is successfully inserts into the table, this function will return the number id of the new task that can be used to delete the task. The complete value of the new task will be set with default value 'false' which means the task is not yet done.

#### ** DELETE method (URL: /todos/:id)**
To delete a single todo task from the todo table using the ID number of the task. The ID number of each task is unique, therefore this method only required the ID number as the task name and the user name can be duplicated.

#### ** PUT method (URL: /todos/:id)** 
To update the information of a single task from the todo table. This method requires all the values for the selected task, so that this method can response any request without knowing which value that need to be update. The ID number of the todo task will be included in the URL address.

#### ** PUT method (URL: /todos/:id/:complete)** 
To update the information of a single task from the todo table when the mouse-dragged events occur. This method required only requires the ID number and the value of the complete property. Both the information will be included in the URL address.

## **Curl Test Cases** ##
###**GET method (the html file)**
> curl http://localhost:8080

###**GET method (all the todo tasks)**
> curl -H "Content-Type: application/json" -X GET http://localhost:8080/todos
###**POST method**
> curl -H "Content-Type: application/json" -X POST -d '{"task": "type your task here", "user":"type the username here"}' http://localhost:8080/todos

###**PUT method (when done box is clicked, then move todo task to complete-list)**
> curl -H "Content-Type: application/json" -X PUT -d '{"task": "type your task here", "user":"type the username here", "complete":"type either false/true"}' http://localhost:8080/todos/id

###**PUT method (mouse-dragged event)**
> curl -H "Content-Type: application/json" -X PUT http://localhost:8080/todos/id/complete

###**DELETE method (delete a todo-task using id)**
>curl -X DELETE http://localhost:8080/todos/ id