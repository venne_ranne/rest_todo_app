var isDragOn = false;
$(document).ready(function(e) {
	$('#add-todo').button({
		icons:{
			primary: "ui-icon-circle-plus",
		}
	}).click(function(){
		$('#new-todo').dialog('open');
		}); //end click function

	$('#new-todo').dialog({
		modal:true,
		autoOpen:false,

		buttons:{
			"Add task":function(){
				var taskName = $('#task').val().toString();
				var userName = $('#user').val().toString();
				if (taskName === ""){
					return false;
				} // end if stmt

				var input = {};
				input["task"] = taskName;
				input["user"] = userName;

				$.ajax({
						type: 'POST',
						url: '/todos',
						data: JSON.stringify(input),
						dataType: 'json',
						contentType: "application/json",
						success: function(data){
							var itemId = ''+data[0].id;  // add the id to the li
							var taskHTML = '<li id = "'+itemId+'"><span class="done">%</span>';
							taskHTML += '<span class="edit">e</span>';
							taskHTML += '<span class="delete">x</span>';
							taskHTML += '<span class="task" contentEditable></span>';
							taskHTML += '<span class="user" contentEditable></span></li>';
							var $newTask = $(taskHTML);
							$newTask.find('.task').text(data[0].item);
							$newTask.find('.user').text(data[0].username);
							$newTask.hide();
							$('#todo-list').prepend($newTask);
							$newTask.show('clip', 250).effect('highlight', 1000);
						}
				});
				$(this).dialog('close');
			}, // end Add task

			"Cancel":function(){
				$(this).dialog('close');
			}
		}, // end buttons

			// to reset the textfield for the next task input
			close: function(){
				$('#new-todo input').val('');
			}
	}); // end dialog

	$('.sortlist').sortable({
		connectWith: '.sortlist',
		cursor: 'pointer',
		placeholder: 'ui-state-highlight',
		cancel: '.delete, .done'
	});

	// pop-up the delete confirmation box when the delete icon is clicked
	$('.sortlist').on('click', '.delete', function(){
		selectedTask = $(this);    // Note: without var keyword, this variable auto global
		$('#delete-confirm').dialog('open');
	});

	// display task in the completed-list and hides the edit button
	$('#todo-list').on('click', '.done', function(){
		var $taskItem = $(this).parent('li');
		var itemId = $(this).parent().attr('id').toString();  // get the id number

		$.ajax({
				type: 'PUT',
				url: '/todos/'+itemId,
				success: function(data){
					$taskItem.slideUp(250, function(){
						var $this = $(this);
						$this.find('.edit').remove();
						$this.detach();
						$('#completed-list').prepend($this);
						$this.slideDown();
					}); // end slideUp
				}
		});

	}); // end on

	// Delete confirmation dialog: Delete the selected task if the user clicked confirm
	$('#delete-confirm').dialog({
		modal:true,
		autoOpen:false,
		buttons:{
			"Confirm":function(){
				var itemId = selectedTask.parent().attr('id').toString();  // get the id number

				// delete a task using id
				$.ajax({
					type: 'DELETE',
					url: '/todos/'+itemId,
					success: function(data){
						selectedTask.parent('li').effect('puff', function(){
							selectedTask.remove();
						});
					}
				});

				$(this).dialog('close');  // close the dialog
			},
			"Cancel":function(){
				$(this).dialog('close');
			}
		}
	});

	// pop-up the delete confirmation box when the delete icon is clicked
	$('.sortlist').on('click', '.edit', function(){
		selectedTask = $(this);    // Note: without var keyword, this variable auto global
		$('#edit-task-dialog').dialog('open');
	});

	// pop-up the edit task dialog box when pencil icon is clicked
	$('#edit-task-dialog').dialog({
		modal:true,
		autoOpen:false,
		buttons:{
			"Save": function(){
				var itemId = selectedTask.parent().attr('id').toString();  // get the id number
				var taskName = $('#edit-task').val();    // get the current value of taskName
				var userName = $('#edit-user').val();	   // get the currrent value of userName

				var input = {};
				input["task"] = taskName;
				input["user"] = userName;
				input["complete"] = 'false';

				$.ajax({
						type: 'PUT',
						url: '/todos/'+itemId,
						data: JSON.stringify(input),
						dataType: 'json',
						contentType: "application/json",
						success: function(data){
							selectedTask.parent('li').find('.task')[0].innerHTML = data[0].item;
							selectedTask.parent('li').find('.user')[0].innerHTML = data[0].username;
						}
				});

				$(this).dialog('close');
			},
			"Cancel":function(){
				$(this).dialog('close');
			}
		},
		open: function(){
			var taskName = selectedTask.parent('li').find('.task')[0].innerText;
			var userName = selectedTask.parent('li').find('.user')[0].innerText;
			$('#edit-task').val(taskName);
			$('#edit-user').val(userName);
		}

	});

  // get all the rows from the
	$.ajax({
		type: 'GET',
		url: '/todos',
		success: function(data){
			for (i = 0; i < data.length; i++) {
				var itemId = ''+data[i].id;  // add the id to the li
				var taskHTML = '<li id = "'+itemId+'"><span class="done">%</span>';
				taskHTML += '<span class="edit">e</span>';
				taskHTML += '<span class="delete">x</span>';
				taskHTML += '<span class="task" contentEditable></span>';
				taskHTML += '<span class="user" contentEditable></span></li>';
				if (data[i].complete == false){
					var $newTask = $(taskHTML);
					$newTask.find('.task').text(data[i].item);
					$newTask.find('.user').text(data[i].username);
					$newTask.hide();
					$('#todo-list').prepend($newTask);
					$newTask.show('clip', 250).effect('highlight', 1000);
				} else {
					var $doneTask= $(taskHTML);
					$doneTask.find('.task').text(data[i].item);
					$doneTask.find('.user').text(data[i].username);
					$doneTask.hide();
					$('#completed-list').prepend($doneTask);
					$doneTask.show('clip', 250).effect('highlight', 1000);
				}
			}
		}

	});

  // functions below for mouse dragging update
	$('.sortlist').on('mousedown', function(event){
		dragId = event.target.id;  // get the id number
	});

	$('.sortlist').on('mouseup', function(event){
		prevList = $(this).attr('id');
		isDragOn = true;
	});

	$('.sortlist').on('mouseleave', function(event){
		if (!isDragOn){
			return false;
		}
		var parent = $(this).attr('id');
		var complete, itemId;

		if (parent == "completed-list" && prevList != parent){
			complete = true;
		} else if (parent == "todo-list" && prevList != parent){
			complete = false;
		} else {
				isDragOn = false;
				return false;
		}
		if (Number.isInteger(parseInt(dragId))){
			itemId = ''+dragId;
		} else if (event.target.nodeName == 'LI'){
			itemId = event.target.id;
		} else if (Number.isInteger(parseInt(event.target.id))){
			itemId = event.target.id;
		} else if (event.target.nodeName == 'SPAN'){
			itemId = event.target.parentNode.id;
		} else {
			isDragOn = false;
			return false;
		}

		$.ajax({
				type: 'PUT',
				url: '/todos/'+itemId+'/'+complete,
				contentType: "application/json",
				success: function(data){
					console.log("success update!");
					console.log(data);
				}
		});
		isDragOn = false;
	});

}); // end ready
